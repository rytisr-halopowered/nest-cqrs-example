install:
	cp .env.dev.properties .env
start:
	docker-compose up backend
setup:
	curl http://localhost:3000/setup