import { existsSync, mkdirSync, readFileSync, writeFileSync } from 'fs';
import { camelCase, kebabCase, upperFirst } from 'lodash';
import { join } from 'path';

// names
const name = kebabCase(process.argv[2]);
const className = upperFirst(camelCase(name));
const dtoName = kebabCase(process.argv[3]);
const dtoClass = upperFirst(camelCase(dtoName)) + 'Dto';

// paths
const rootDir = process.cwd();
const templateDir = join(__dirname, 'templates');
const moduleDir = join(rootDir, 'src', 'domains', name);
const dtoDir = join(moduleDir, 'dtos');
const dtoFile = join(dtoDir, `${dtoName}.dto.ts`);

// templates
const dtoTemplate = readFileSync(join(templateDir, 'dto.txt')).toString();

// regex
const DTO_CLASS_REGEX = /\{\{DTO_CLASS\}\}/g;

if (process.argv.length < 4) {
  console.log('Usage: yarn g:mo:d [module] [dto]');
  process.exit(1);
}

if (!existsSync(moduleDir)) {
  console.error(`Module ${moduleDir} does not exist!`);
  process.exit(1);
}

if (existsSync(dtoFile)) {
  console.error(`File ${dtoFile} already exists!`);
  process.exit(1);
}

if (!existsSync(dtoDir)) {
  mkdirSync(dtoDir, 0o755);
}

// dto.ts
writeFileSync(dtoFile, dtoTemplate.replace(DTO_CLASS_REGEX, dtoClass));
