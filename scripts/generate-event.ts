import { existsSync, mkdirSync, readFileSync, writeFileSync } from 'fs';
import { camelCase, kebabCase, upperFirst } from 'lodash';
import { join } from 'path';

// names
const name = kebabCase(process.argv[2]);
const className = upperFirst(camelCase(name));
const eventName = kebabCase(process.argv[3]);
const eventClass = upperFirst(camelCase(eventName)) + 'Event';

// paths
const rootDir = process.cwd();
const templateDir = join(__dirname, 'templates');
const moduleDir = join(rootDir, 'src', 'domains', name);
const eventsDir = join(moduleDir, 'events');
const eventFile = join(eventsDir, `${eventName}.event.ts`);

// templates
const eventTemplate = readFileSync(join(templateDir, 'event.txt')).toString();

// regex
const EVENT_CLASS_REGEX = /\{\{EVENT_CLASS\}\}/g;

if (process.argv.length < 4) {
  console.log('Usage: yarn g:mo:d [module] [event]');
  process.exit(1);
}

if (!existsSync(moduleDir)) {
  console.error(`Module ${moduleDir} does not exist!`);
  process.exit(1);
}

if (existsSync(eventFile)) {
  console.error(`File ${eventFile} already exists!`);
  process.exit(1);
}

if (!existsSync(eventsDir)) {
  mkdirSync(eventsDir, 0o755);
}

// event.ts
writeFileSync(eventFile, eventTemplate.replace(EVENT_CLASS_REGEX, eventClass));
