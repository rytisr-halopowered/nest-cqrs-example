import { Module, OnModuleInit } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { CommandBus, EventBus, CQRSModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGO_WRITE } from '../../shared/mongo';
import { {{CLASS_NAME}}Schema, WRITE_MODEL_{{MODEL_NAME}} } from './{{NAME}}.model';
import { {{CLASS_NAME}}Repository } from './{{NAME}}.repository';
import CommandHandlers from './commands';
import { {{CLASS_NAME}}Controller } from './controllers/{{NAME}}.controller';

@Module({
  imports: [
    CQRSModule,
    MongooseModule.forFeature(
      [{ name: WRITE_MODEL_{{MODEL_NAME}}, schema: {{CLASS_NAME}}Schema }],
      MONGO_WRITE,
    ),
  ],
  providers: [{{CLASS_NAME}}Repository, ...CommandHandlers],
  controllers: [{{CLASS_NAME}}Controller],
  exports: [{{CLASS_NAME}}Repository],
})
export class {{CLASS_NAME}}DomainModule implements OnModuleInit {
  constructor(
    private moduleRef: ModuleRef,
    private command$: CommandBus,
    private event$: EventBus,
  ) {}

  onModuleInit() {
    this.command$.setModuleRef(this.moduleRef);
    this.event$.setModuleRef(this.moduleRef);

    this.command$.register(CommandHandlers);
  }
}
