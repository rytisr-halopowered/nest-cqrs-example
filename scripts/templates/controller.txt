import { Controller } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';

@Controller('{{NAME}}')
export class {{CLASS_NAME}}Controller {
  constructor(private commands$: CommandBus) {}
}
