import { camelCase, kebabCase, snakeCase, upperFirst } from 'lodash';
import { join } from 'path';
import { existsSync, readFileSync, writeFileSync, mkdirSync } from 'fs';

// names
const name = kebabCase(process.argv[2]);
const className = upperFirst(camelCase(name));
const modelName = snakeCase(name).toUpperCase();

// paths
const rootDir = process.cwd();
const templateDir = join(__dirname, 'templates');
const domainsDir = join(rootDir, 'src', 'domains');
const domainsModule = join(rootDir, 'src', 'domains', 'domains.module.ts');
const moduleDir = join(domainsDir, name);
const moduleModule = join(moduleDir, `${name}.module.ts`);
const moduleRoot = join(moduleDir, `${name}.root.ts`);
const moduleRepository = join(moduleDir, `${name}.repository.ts`);
const moduleModel = join(moduleDir, `${name}.model.ts`);
const controllerDir = join(moduleDir, 'controllers');
const moduleController = join(controllerDir, `${name}.controller.ts`);
const commandsDir = join(moduleDir, 'commands');

// templates
const moduleTemplate = readFileSync(join(templateDir, 'module.txt')).toString();
const rootTemplate = readFileSync(join(templateDir, 'root.txt')).toString();
const modelTemplate = readFileSync(join(templateDir, 'model.txt')).toString();
const repositoryTemplate = readFileSync(
  join(templateDir, 'repository.txt'),
).toString();
const controllerTemplate = readFileSync(
  join(templateDir, 'controller.txt'),
).toString();

// regex
const NAME_REGEX = /\{\{NAME\}\}/g;
const CLASS_NAME_REGEX = /\{\{CLASS_NAME\}\}/g;
const MODEL_NAME_REGEX = /\{\{MODEL_NAME\}\}/g;

if (process.argv.length < 3) {
  console.log('Usage: yarn g:mo:d [name]');
  process.exit(1);
}

if (existsSync(moduleDir)) {
  console.error(`Path ${moduleDir} already exists!`);
  process.exit(1);
}

// mkdir
mkdirSync(moduleDir, 0o755);
mkdirSync(controllerDir, 0o755);
mkdirSync(commandsDir, 0o755);
// module.ts
writeFileSync(
  moduleModule,
  moduleTemplate
    .replace(NAME_REGEX, name)
    .replace(CLASS_NAME_REGEX, className)
    .replace(MODEL_NAME_REGEX, modelName),
);
// repository.ts
writeFileSync(
  moduleRepository,
  repositoryTemplate
    .replace(NAME_REGEX, name)
    .replace(CLASS_NAME_REGEX, className)
    .replace(MODEL_NAME_REGEX, modelName),
);
// root.ts
writeFileSync(
  moduleRoot,
  rootTemplate
    .replace(NAME_REGEX, name)
    .replace(CLASS_NAME_REGEX, className)
    .replace(MODEL_NAME_REGEX, modelName),
);
// model.ts
writeFileSync(
  moduleModel,
  modelTemplate
    .replace(NAME_REGEX, name)
    .replace(CLASS_NAME_REGEX, className)
    .replace(MODEL_NAME_REGEX, modelName),
);
// controller.ts
writeFileSync(
  moduleController,
  controllerTemplate
    .replace(NAME_REGEX, name)
    .replace(CLASS_NAME_REGEX, className)
    .replace(MODEL_NAME_REGEX, modelName),
);
// commands
writeFileSync(
  join(commandsDir, 'index.ts'),
  `const CommandHandlers = [];\nexport default CommandHandlers;`,
);

// append to domain module
const index = readFileSync(domainsModule).toString();
writeFileSync(
  domainsModule,
  `import { ${className}DomainModule } from './${name}/${name}.module';\n` +
    index.replace(
      /imports:\s\[(.*?)(\])/ms,
      (match: string, current: string = '', end: string = '') => {
        if (!current.trim()) {
          return `imports: [${className}DomainModule]`;
        } else if (current.match(/\,\s*$/ms)) {
          return `imports: [${current}\n  ${className}DomainModule,\n]`;
        } else {
          return `imports: [${current}, ${className}DomainModule]`;
        }
      },
    ),
);
