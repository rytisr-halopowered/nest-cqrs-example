import { existsSync, mkdirSync, readFileSync, writeFileSync } from 'fs';
import { camelCase, kebabCase, upperFirst } from 'lodash';
import { join } from 'path';

// names
const name = kebabCase(process.argv[2]);
const className = upperFirst(camelCase(name));
const commandName = kebabCase(process.argv[3]);
const commandClass = upperFirst(camelCase(commandName)) + 'Command';

// paths
const rootDir = process.cwd();
const templateDir = join(__dirname, 'templates');
const moduleDir = join(rootDir, 'src', 'domains', name);
const commandsDir = join(moduleDir, 'commands');
const commandFile = join(commandsDir, `${commandName}.command.ts`);
const indexFile = join(commandsDir, 'index.ts');

// templates
const commandTemplate = readFileSync(
  join(templateDir, 'command.txt'),
).toString();

// regex
const NAME_REGEX = /\{\{NAME\}\}/g;
const CLASS_NAME_REGEX = /\{\{CLASS_NAME\}\}/g;
const COMMAND_CLASS_REGEX = /\{\{COMMAND_CLASS\}\}/g;

if (process.argv.length < 4) {
  console.log('Usage: yarn g:mo:d [module] [command]');
  process.exit(1);
}

if (!existsSync(moduleDir)) {
  console.error(`Module ${moduleDir} does not exist!`);
  process.exit(1);
}

if (existsSync(commandFile)) {
  console.error(`File ${commandFile} already exists!`);
  process.exit(1);
}

if (!existsSync(commandsDir)) {
  mkdirSync(commandsDir, 0o755);
}

// command.ts
writeFileSync(
  commandFile,
  commandTemplate
    .replace(NAME_REGEX, name)
    .replace(CLASS_NAME_REGEX, className)
    .replace(COMMAND_CLASS_REGEX, commandClass),
);

const index = readFileSync(indexFile).toString();
writeFileSync(
  indexFile,
  `import { ${commandClass}Handler } from './${commandName}.command';\n` +
    index.replace(
      /CommandHandlers\s=\s\[(.*?)(\])/ms,
      (match: string, current: string = '', end: string = '') => {
        if (!current.trim()) {
          return `CommandHandlers = [${commandClass}Handler]`;
        } else if (current.match(/\,\s*$/ms)) {
          return `CommandHandlers = [${current}\n  ${commandClass}Handler,\n]`;
        } else {
          return `CommandHandlers = [${current}, ${commandClass}Handler]`;
        }
      },
    ),
);
