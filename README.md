# CQRS/ES nestjs example app

This is an example project of structure and best practices when implementing CQRS and event sourcing in projects. 

## Features

- Generators for domain module elements (commands, events, etc.)
- Command retry when concurrency error detected.
- Distributed event handling with RabbitMQ.

## Examples

- Validation with one aggregate
- Validation with one related aggregate
- Validation with multiple related aggregates
- Building a simple projection

## Known issues

- Inconsistent view updates
- Insert slowdown overtime
- Missing types in `onEvent` switch cases

## Install
```
yarn
make install
```
## Run
```
make start
```