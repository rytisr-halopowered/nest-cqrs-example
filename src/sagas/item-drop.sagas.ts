import { Injectable } from '@nestjs/common';
import { Saga } from '@nestjs/cqrs';
import { DragonKilledEvent } from '../domains/dragon/events/dragon-killed.event';
import { delay, map } from 'rxjs/operators';
import { PickupItemCommand } from '../domains/hero/commands/pickup-item.command';
import { Observable } from 'rxjs';

@Injectable()
export class ItemDropSagas {
  ancientItem$: Saga = events$ =>
    (events$.ofType(DragonKilledEvent) as Observable<DragonKilledEvent>).pipe(
      delay(1000),
      map(
        ({ heroId }: DragonKilledEvent) =>
          new PickupItemCommand(heroId, 'ancient-item'),
      ),
    )
}
