import { Module, OnModuleInit } from '@nestjs/common';
import { CQRSModule, EventBus } from '@nestjs/cqrs';
import { ItemDropSagas } from './item-drop.sagas';

@Module({
  imports: [CQRSModule],
  providers: [ItemDropSagas],
})
export class SagasModule implements OnModuleInit {
  constructor(private events$: EventBus, private itemDrop: ItemDropSagas) {}

  onModuleInit() {
    this.events$.combineSagas([this.itemDrop.ancientItem$]);
  }
}
