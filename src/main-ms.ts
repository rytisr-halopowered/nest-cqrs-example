import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app.module';
import { RABBIT_OPTIONS } from './shared/rabbit';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(
    ApplicationModule,
    RABBIT_OPTIONS,
  );
  app.listen(() => console.log('Application listening RabbitMQ queue.'));
}
bootstrap();
