import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { RABBIT_OPTIONS } from './shared/rabbit';

async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule);
  const rabbit = app.connectMicroservice(RABBIT_OPTIONS);
  app.useGlobalPipes(new ValidationPipe());
  rabbit.listen(() => console.log('Application listening RabbitMQ queue.'));
  app.listen(3000, () => console.log('Application is listening on port 3000.'));
}
bootstrap();
