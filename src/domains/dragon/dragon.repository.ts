import { Inject, Injectable } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CommitRepository } from '../../shared/cqrs/commit.repository';
import { DragonModel, WRITE_MODEL_DRAGON } from './dragon.model';
import { Dragon } from './dragon.root';
import { CommitSerializer } from '../../shared/cqrs/commit.serializer';

@Injectable()
export class DragonRepository extends CommitRepository<Dragon> {
  constructor(
    @Inject(getModelToken(WRITE_MODEL_DRAGON))
    model: Model<DragonModel>,
  ) {
    super(Dragon, model);
  }

  public async findDragonsInSpawn(spawn: string) {
    const commits: DragonModel[] = await this._model
      .find({ spawn })
      .sort('version')
      .lean()
      .exec();
    const result: { [id: string]: Dragon } = {};
    commits.forEach(commit => {
      const root =
        result[commit.aggregateId] || new this._root(commit.aggregateId, 1);
      result[commit.aggregateId] = root;
      root.version = Math.max(root.version, commit.version);
      root.loadFromHistory(commit.events.map(CommitSerializer.deserialize));
    });
    return Object.values(result).filter(x => x.exists);
  }

  protected getAggregateFields(root: Dragon) {
    return {
      spawn: root.spawn,
    };
  }
}
