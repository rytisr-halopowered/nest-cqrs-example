import {
  CommitModel,
  createCommitSchema,
} from '../../shared/cqrs/commit.model';

export const WRITE_MODEL_DRAGON = 'Dragon';
export interface DragonModel extends CommitModel {
  spawn: string;
}
export const DragonSchema = createCommitSchema({
  spawn: String,
});
DragonSchema.index({ spawn: true });
