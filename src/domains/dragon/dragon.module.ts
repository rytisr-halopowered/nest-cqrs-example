import { Module, OnModuleInit } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { CommandBus, CQRSModule, EventBus } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGO_WRITE } from '../../shared/mongo';
import { HeroDomainModule } from '../hero/hero.module';
import CommandHandlers from './commands';
import { DragonController } from './controllers/dragon.controller';
import { DragonSchema, WRITE_MODEL_DRAGON } from './dragon.model';
import { DragonRepository } from './dragon.repository';

@Module({
  imports: [
    CQRSModule,
    HeroDomainModule,
    MongooseModule.forFeature(
      [{ name: WRITE_MODEL_DRAGON, schema: DragonSchema }],
      MONGO_WRITE,
    ),
  ],
  providers: [DragonRepository, ...CommandHandlers],
  controllers: [DragonController],
  exports: [DragonRepository],
})
export class DragonDomainModule implements OnModuleInit {
  constructor(
    private moduleRef: ModuleRef,
    private command$: CommandBus,
    private event$: EventBus,
  ) {}

  onModuleInit() {
    this.command$.setModuleRef(this.moduleRef);
    this.event$.setModuleRef(this.moduleRef);

    this.command$.register(CommandHandlers);
  }
}
