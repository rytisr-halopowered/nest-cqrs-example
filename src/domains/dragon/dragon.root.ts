import { CommitRoot } from '../../shared/cqrs/commit.root';
import { IEvent } from '@nestjs/cqrs';
import { DragonSpawnedEvent } from './events/dragon-spawned.event';
import { DragonKilledEvent } from './events/dragon-killed.event';

export class Dragon extends CommitRoot {
  public spawn?: string;
  public exists: boolean = false;
  public onEvent(event: IEvent) {
    switch (this._getEventName(event)) {
      case DragonSpawnedEvent.name:
        return this.onSpawn(event as DragonSpawnedEvent);
      case DragonKilledEvent.name:
        this.exists = false;
        break;
    }
  }
  private onSpawn(event: DragonSpawnedEvent) {
    this.spawn = event.spawn;
    this.exists = true;
  }
}
