import { Controller, Post, Body, Delete, Param } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { SpawnDragonDto } from '../dtos/spawn-dragon.dto';
import { SpawnDragonCommand } from '../commands/spawn-dragon.command';
import { KillDragonDto } from '../dtos/kill-dragon.dto';
import { KillDragonCommand } from '../commands/kill-dragon.command';

@Controller('dragon')
export class DragonController {
  constructor(private commands$: CommandBus) {}

  @Post()
  public async store(@Body() { spawn }: SpawnDragonDto) {
    return this.commands$.execute(new SpawnDragonCommand(spawn));
  }
  @Delete(':dragon')
  public async destroy(
    @Body() { heroId }: KillDragonDto,
    @Param('dragon') dragonId: string,
  ) {
    return this.commands$.execute(new KillDragonCommand(dragonId, heroId));
  }
}
