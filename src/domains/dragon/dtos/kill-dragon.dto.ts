import { IsString } from 'class-validator';

export class KillDragonDto {
  @IsString()
  public heroId!: string;
}
