import { IsIn } from 'class-validator';

export class SpawnDragonDto {
  @IsIn(['desert', 'forest', 'village'])
  public spawn!: string;
}
