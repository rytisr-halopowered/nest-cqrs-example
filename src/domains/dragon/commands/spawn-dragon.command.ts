import { CommitHandler } from '../../../shared/cqrs/commit.handler';
import { Dragon } from '../dragon.root';
import { DragonRepository } from '../dragon.repository';
import { CommandHandler, ICommand } from '@nestjs/cqrs';
import { DragonSpawnedEvent } from '../events/dragon-spawned.event';

export class SpawnDragonCommand implements ICommand {
  constructor(public spawn: string) {}
}

@CommandHandler(SpawnDragonCommand)
export class SpawnDragonCommandHandler extends CommitHandler<
  Dragon,
  SpawnDragonCommand
> {
  constructor(protected repository: DragonRepository) {
    super();
  }
  public async handle({ spawn }: SpawnDragonCommand): Promise<Dragon> {
    const root = await this.repository.create();
    const dragons = await this.repository.findDragonsInSpawn(spawn);

    if (dragons.length >= 5) {
      throw new Error('To many dragons!');
    }

    root.apply(new DragonSpawnedEvent(spawn, root.aggregateId));

    return root;
  }
}
