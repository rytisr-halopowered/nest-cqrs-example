import { KillDragonCommandHandler } from './kill-dragon.command';
import { SpawnDragonCommandHandler } from './spawn-dragon.command';
const CommandHandlers = [SpawnDragonCommandHandler, KillDragonCommandHandler];
export default CommandHandlers;