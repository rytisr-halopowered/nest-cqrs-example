import { CommitHandler } from '../../../shared/cqrs/commit.handler';
import { Dragon } from '../dragon.root';
import { DragonRepository } from '../dragon.repository';
import { CommandHandler, ICommand } from '@nestjs/cqrs';
import { DragonKilledEvent } from '../events/dragon-killed.event';
import { HeroRepository } from '../../hero/hero.repository';

export class KillDragonCommand implements ICommand {
  constructor(public dragonId: string, public heroId: string) {}
}

@CommandHandler(KillDragonCommand)
export class KillDragonCommandHandler extends CommitHandler<
  Dragon,
  KillDragonCommand
> {
  constructor(
    protected repository: DragonRepository,
    private heroRepository: HeroRepository,
  ) {
    super();
  }
  public async handle({
    dragonId,
    heroId,
  }: KillDragonCommand): Promise<Dragon> {
    const root = await this.repository.findAndRestore(dragonId);
    const heroRoot = await this.heroRepository.findAndRestore(heroId);

    if (!heroRoot.exists) {
      throw new Error('Hero not found!');
    }

    if (!root.exists) {
      throw new Error('Dragon not found!');
    }

    root.apply(new DragonKilledEvent(dragonId, heroId));

    return root;
  }
}
