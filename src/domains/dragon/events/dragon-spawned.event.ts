import { IEvent } from '@nestjs/cqrs';

export class DragonSpawnedEvent implements IEvent {
  constructor(public spawn: string, public dragonId: string) {}
}
