import { IEvent } from '@nestjs/cqrs';

export class DragonKilledEvent implements IEvent {
  constructor(public dragonId: string, public heroId: string) {}
}
