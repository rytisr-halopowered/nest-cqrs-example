import {
  CommitModel,
  createCommitSchema,
} from '../../shared/cqrs/commit.model';

export const WRITE_MODEL_HERO = 'Hero';
export interface HeroModel extends CommitModel {}
export const HeroSchema = createCommitSchema();
