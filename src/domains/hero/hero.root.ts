import { CommitRoot } from '../../shared/cqrs/commit.root';
import { IEvent } from '@nestjs/cqrs';
import { HeroSpawnedEvent } from './events/hero-spawned.event';

export class Hero extends CommitRoot {
  public exists: boolean = false;
  public onEvent(event: IEvent) {
    switch (this._getEventName(event)) {
      case HeroSpawnedEvent.name:
        this.exists = true;
        break;
    }
  }
}
