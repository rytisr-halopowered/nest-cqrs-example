import { CommitHandler } from '../../../shared/cqrs/commit.handler';
import { Hero } from '../hero.root';
import { HeroRepository } from '../hero.repository';
import { CommandHandler, ICommand } from '@nestjs/cqrs';
import { HeroSpawnedEvent } from '../events/hero-spawned.event';

export class SpawnHeroCommand implements ICommand {
}

@CommandHandler(SpawnHeroCommand)
export class SpawnHeroCommandHandler extends CommitHandler<Hero, SpawnHeroCommand> {
  constructor(protected repository: HeroRepository) {
    super();
  }
  public async handle(command: SpawnHeroCommand): Promise<Hero> {
    const root = await this.repository.create();

    root.apply(new HeroSpawnedEvent());

    return root;
  }
}
