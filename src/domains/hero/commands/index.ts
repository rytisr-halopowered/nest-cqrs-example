import { PickupItemCommandHandler } from './pickup-item.command';
import { SpawnHeroCommandHandler } from './spawn-hero.command';
const CommandHandlers = [SpawnHeroCommandHandler, PickupItemCommandHandler];
export default CommandHandlers;