import { CommitHandler } from '../../../shared/cqrs/commit.handler';
import { Hero } from '../hero.root';
import { HeroRepository } from '../hero.repository';
import { CommandHandler, ICommand } from '@nestjs/cqrs';
import { HeroPickedUpItemEvent } from '../events/hero-picked-up-item.event';

export class PickupItemCommand implements ICommand {
  constructor(public heroId: string, public itemId: string) {}
}

@CommandHandler(PickupItemCommand)
export class PickupItemCommandHandler extends CommitHandler<
  Hero,
  PickupItemCommand
> {
  constructor(protected repository: HeroRepository) {
    super();
  }
  public async handle({ heroId, itemId }: PickupItemCommand): Promise<Hero> {
    const root = await this.repository.findAndRestore(heroId);

    if (!root.exists) {
      throw new Error('Hero not found!');
    }

    root.apply(new HeroPickedUpItemEvent(itemId));

    return root;
  }
}
