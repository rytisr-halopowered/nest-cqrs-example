import { Controller, Post } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { SpawnHeroCommand } from '../commands/spawn-hero.command';

@Controller('hero')
export class HeroController {
  constructor(private commands$: CommandBus) {}

  @Post()
  public async store() {
    return this.commands$.execute(new SpawnHeroCommand());
  }
}
