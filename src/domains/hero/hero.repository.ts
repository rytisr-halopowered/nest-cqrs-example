import { Inject, Injectable } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CommitRepository } from '../../shared/cqrs/commit.repository';
import { HeroModel, WRITE_MODEL_HERO } from './hero.model';
import { Hero } from './hero.root';

@Injectable()
export class HeroRepository extends CommitRepository<Hero> {
  constructor(
    @Inject(getModelToken(WRITE_MODEL_HERO))
    model: Model<HeroModel>,
  ) {
    super(Hero, model);
  }
}
