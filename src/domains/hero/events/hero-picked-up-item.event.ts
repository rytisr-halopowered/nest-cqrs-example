import { IEvent } from '@nestjs/cqrs';

export class HeroPickedUpItemEvent implements IEvent {
  constructor(public itemId: string) {}
}
