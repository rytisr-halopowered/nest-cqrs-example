import { Module, OnModuleInit } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { CommandBus, CQRSModule, EventBus } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { MONGO_WRITE } from '../../shared/mongo';
import CommandHandlers from './commands';
import { HeroController } from './controllers/hero.controller';
import { HeroSchema, WRITE_MODEL_HERO } from './hero.model';
import { HeroRepository } from './hero.repository';

@Module({
  imports: [
    CQRSModule,
    MongooseModule.forFeature(
      [{ name: WRITE_MODEL_HERO, schema: HeroSchema }],
      MONGO_WRITE,
    ),
  ],
  providers: [HeroRepository, ...CommandHandlers],
  controllers: [HeroController],
  exports: [HeroRepository],
})
export class HeroDomainModule implements OnModuleInit {
  constructor(
    private moduleRef: ModuleRef,
    private command$: CommandBus,
    private event$: EventBus,
  ) {}

  onModuleInit() {
    this.command$.setModuleRef(this.moduleRef);
    this.event$.setModuleRef(this.moduleRef);

    this.command$.register(CommandHandlers);
  }
}
