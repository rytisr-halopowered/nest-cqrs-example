import { HeroDomainModule } from './hero/hero.module';
import { DragonDomainModule } from './dragon/dragon.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [DragonDomainModule, HeroDomainModule],
})
export class DomainsModule {}
