import { Logger, Module, OnModuleInit } from '@nestjs/common';
import { CommandBus, CQRSModule, EventBus } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { DomainsModule } from './domains/domains.module';
import { SagasModule } from './sagas/sagas.module';
import { CommitModule } from './shared/cqrs/commit.module';
import { ViewsModule } from './views/views.module';

@Module({
  imports: [
    CQRSModule,
    CommitModule,
    MongooseModule.forRoot(process.env.MONGO_WRITE_URL || '', {
      connectionName: 'write',
      useNewUrlParser: true,
    }),
    MongooseModule.forRoot(process.env.MONGO_READ_URL || '', {
      connectionName: 'read',
      useNewUrlParser: true,
    }),
    DomainsModule,
    SagasModule,
    ViewsModule,
  ],
  controllers: [AppController],
})
export class ApplicationModule implements OnModuleInit {
  constructor(private commands$: CommandBus, private events$: EventBus) {}

  public onModuleInit() {
    this.commands$.subscribe(command =>
      Logger.log(
        `${Object.getPrototypeOf(command).constructor.name} ${JSON.stringify(
          command,
        )}`,
        'commands$',
      ),
    );
    this.events$.subscribe(event =>
      Logger.log(
        `${Object.getPrototypeOf(event).constructor.name} ${JSON.stringify(
          event,
        )}`,
        'events$',
      ),
    );
  }
}
