import { Controller, Get, Inject } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { getConnectionToken } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { KillDragonCommand } from './domains/dragon/commands/kill-dragon.command';
import { SpawnDragonCommand } from './domains/dragon/commands/spawn-dragon.command';
import { SpawnHeroCommand } from './domains/hero/commands/spawn-hero.command';
import { AggregateRef } from './shared/cqrs/commit.repository';
import { MONGO_READ, MONGO_WRITE } from './shared/mongo';

@Controller()
export class AppController {
  @Inject(getConnectionToken(MONGO_WRITE))
  private writeConnection!: Connection;
  @Inject(getConnectionToken(MONGO_READ))
  private readConnection!: Connection;

  constructor(private commands$: CommandBus) {}

  @Get('setup')
  public async setup() {
    const hero: AggregateRef = await this.commands$.execute(
      new SpawnHeroCommand(),
    );
    const dragon1: AggregateRef = await this.commands$.execute(
      new SpawnDragonCommand('village'),
    );
    const dragon2: AggregateRef = await this.commands$.execute(
      new SpawnDragonCommand('village'),
    );
    await this.commands$.execute(
      new KillDragonCommand(dragon2.aggregateId, hero.aggregateId),
    );
  }

  @Get('drop-db')
  public async dropDb() {
    await Promise.all([
      ...Object.values(this.writeConnection.collections).map(collection =>
        collection.remove({}),
      ),
      ...Object.values(this.readConnection.collections).map(collection =>
        collection.remove({}),
      ),
    ]);
    return 'OK!';
  }
}
