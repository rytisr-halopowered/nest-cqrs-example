import { Module, OnModuleInit } from '@nestjs/common';
import { CQRSModule, EventBus } from '@nestjs/cqrs';
import { ModuleRef } from '@nestjs/core';
import { MongooseModule } from '@nestjs/mongoose';
import { READ_MODEL_SPAWN, SpawnSchema } from './spawn.model';
import { MONGO_READ } from '../../shared/mongo';
import { SpawnRepository } from './spawn.repository';
import { EventHandlers } from './event-handlers';

@Module({
  imports: [
    CQRSModule,
    MongooseModule.forFeature(
      [{ name: READ_MODEL_SPAWN, schema: SpawnSchema }],
      MONGO_READ,
    ),
  ],
  providers: [SpawnRepository, ...EventHandlers],
  exports: [SpawnRepository],
})
export class SpawnModule implements OnModuleInit {
  constructor(private moduleRef: ModuleRef, private events$: EventBus) {}

  public onModuleInit() {
    this.events$.setModuleRef(this.moduleRef);

    this.events$.register(EventHandlers);
  }
}
