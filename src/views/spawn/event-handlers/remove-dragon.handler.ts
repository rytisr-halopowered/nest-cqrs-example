import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DragonKilledEvent } from '../../../domains/dragon/events/dragon-killed.event';
import { SpawnRepository } from '../spawn.repository';

@EventsHandler(DragonKilledEvent)
export class RemoveDragonHandler implements IEventHandler<DragonKilledEvent> {
  constructor(private repository: SpawnRepository) {}

  public async handle(event: DragonKilledEvent) {
    return this.repository.removeDragon(event.dragonId);
  }
}
