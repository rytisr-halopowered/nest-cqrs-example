import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DragonSpawnedEvent } from '../../../domains/dragon/events/dragon-spawned.event';
import { SpawnRepository } from '../spawn.repository';

@EventsHandler(DragonSpawnedEvent)
export class SpawnDragonHandler implements IEventHandler<DragonSpawnedEvent> {
  constructor(private repository: SpawnRepository) {}

  public async handle(event: DragonSpawnedEvent) {
    return this.repository.pushDragon(event.spawn, event.dragonId);
  }
}
