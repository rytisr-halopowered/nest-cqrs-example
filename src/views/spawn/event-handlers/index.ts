import { SpawnDragonHandler } from './spawn-dragon.handler';
import { RemoveDragonHandler } from './remove-dragon.handler';

export const EventHandlers = [SpawnDragonHandler, RemoveDragonHandler];
