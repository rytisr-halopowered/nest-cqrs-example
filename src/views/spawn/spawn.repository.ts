import { Inject } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { READ_MODEL_SPAWN, SpawnModel } from './spawn.model';

export class SpawnRepository {
  constructor(
    @Inject(getModelToken(READ_MODEL_SPAWN))
    private model: Model<SpawnModel>,
  ) {}

  public async pushDragon(spawn: string, dragonId: string) {
    return this.model.collection.findOneAndUpdate(
      { name: spawn },
      {
        $setOnInsert: { name: spawn },
        $push: { dragons: dragonId },
      },
      { upsert: true },
    );
  }

  public async removeDragon(dragonId: string) {
    return this.model.collection.findOneAndUpdate(
      { dragons: dragonId },
      { $pull: { dragons: dragonId } },
    );
  }
}
