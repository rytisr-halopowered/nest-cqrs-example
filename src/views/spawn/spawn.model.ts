import { Schema, Document } from 'mongoose';

export const READ_MODEL_SPAWN = 'Spawn';
export interface SpawnModel extends Document {
  dragons?: string[];
}
export const SpawnSchema = new Schema({
  name: { type: String, unique: true },
  dragons: [String],
});
