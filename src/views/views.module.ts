import { Module } from '@nestjs/common';
import { SpawnModule } from './spawn/spawn.module';

@Module({
  imports: [SpawnModule],
})
export class ViewsModule {}
