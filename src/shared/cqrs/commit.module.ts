import { EventBus, CQRSModule } from '@nestjs/cqrs';
import { CommitPublisher } from './commit.publisher';
import { OnModuleInit, Module } from '@nestjs/common';
import { CommitController } from './commit.controller';

@Module({
  imports: [CQRSModule],
  providers: [CommitPublisher],
  controllers: [CommitController],
  exports: [CommitPublisher],
})
export class CommitModule implements OnModuleInit {
  constructor(private publisher: CommitPublisher, private events$: EventBus) {}
  public onModuleInit() {
    this.events$.publisher = this.publisher;
  }
}
