import { Type } from '@nestjs/common';
import { IEvent } from '@nestjs/cqrs';
import { Model, Types } from 'mongoose';
import { CommitModel, SerializedEvent } from './commit.model';
import { CommitRoot } from './commit.root';
import { CommitSerializer } from './commit.serializer';

export interface AggregateRef {
  aggregateId: string;
  version: number;
  timestamp: Date;
}
export abstract class CommitRepository<R extends CommitRoot> {
  constructor(protected _root: Type<R>, protected _model: Model<CommitModel>) {}

  public async create(): Promise<R> {
    return new this._root(new Types.ObjectId().toHexString());
  }

  public async findShallow(id: string): Promise<R> {
    const latest = await this._model
      .findOne({ aggregateId: id }, 'version')
      .sort('-version')
      .lean()
      .exec();
    if (latest) {
      return new this._root(id, latest.version + 1);
    } else {
      return new this._root(id);
    }
  }

  public async findAndRestore(id: string): Promise<R> {
    const commits: CommitModel[] = await this._model
      .find({ aggregateId: id })
      .sort('version')
      .lean()
      .exec();
    const root = new this._root(
      id,
      commits.length > 0 ? commits[commits.length - 1].version + 1 : 1,
    );

    commits.forEach(commit => {
      root.loadFromHistory(commit.events.map(CommitSerializer.deserialize));
    });

    return root;
  }

  public async persist(root: R): Promise<AggregateRef> {
    const { aggregateId, version } = root;
    const events = root.getUncommittedEvents().map(CommitSerializer.serialize);
    const timestamp = new Date();
    if (events.length > 0) {
      await this._model.create({
        aggregateId,
        version,
        events,
        timestamp,
        ...this.getAggregateFields(root),
      });
    }
    return { aggregateId, version, timestamp };
  }

  protected getAggregateFields(root: R): object {
    return {};
  }
}
