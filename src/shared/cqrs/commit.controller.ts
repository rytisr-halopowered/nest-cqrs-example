import { Controller, Inject, Logger } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { SerializedEvent } from './commit.model';
import { CommitSerializer } from './commit.serializer';
import { EventBus } from '@nestjs/cqrs';

@Controller()
export class CommitController {
  @Inject()
  private events$!: EventBus;

  @MessagePattern({ type: 'event' })
  public async events(payload: SerializedEvent) {
    Logger.log(JSON.stringify(payload), CommitController.name);
    const event = CommitSerializer.deserialize(payload);
    // tslint:disable-next-line:no-string-literal
    this.events$['subject$'].next(event);
  }
}
