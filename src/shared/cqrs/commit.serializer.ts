import { IEvent } from '@nestjs/cqrs';
import { SerializedEvent } from './commit.model';

export class CommitSerializer {
  public static serialize(event: IEvent): SerializedEvent {
    return { ...event, type: Object.getPrototypeOf(event).constructor.name };
  }
  public static deserialize({ type, ...value }: SerializedEvent): IEvent {
    if (!type) throw new Error('Invalid event!');

    const tmp = { [type]: () => {} };
    const result = Object.create({ constructor: tmp[type] });

    Object.assign(result, value);

    return result;
  }
}
