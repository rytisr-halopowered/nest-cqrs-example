import { IEvent } from '@nestjs/cqrs';
import { Client, ClientProxy } from '@nestjs/microservices';
import { RABBIT_OPTIONS } from '../rabbit';
import { CommitSerializer } from './commit.serializer';

export class CommitPublisher {
  @Client(RABBIT_OPTIONS)
  private client!: ClientProxy;

  public async publish(event: IEvent) {
    const payload = CommitSerializer.serialize(event);
    await this.client.send({ type: 'event' }, payload).toPromise();
  }
}
