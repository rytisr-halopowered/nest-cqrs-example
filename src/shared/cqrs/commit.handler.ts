import { Inject } from '@nestjs/common';
import { EventPublisher, ICommand, ICommandHandler } from '@nestjs/cqrs';
import { CommitRepository } from './commit.repository';
import { CommitRoot } from './commit.root';

export abstract class CommitHandler<R extends CommitRoot, C extends ICommand>
  implements ICommandHandler<C> {
  @Inject()
  private publisher!: EventPublisher;

  protected abstract repository: CommitRepository<R>;

  public abstract async handle(command: C): Promise<R>;

  public async execute(command: C, resolve: (value?: any) => void) {
    let i = 0;
    do {
      try {
        const root = await this.handle(command);

        resolve(await this.repository.persist(root));

        this.publisher.mergeObjectContext(root);

        return root.commit();
      } catch (error) {
        if (i < 5 && error.code === 11000) {
          // duplicate key error
          i++;
          continue;
        }
        return resolve(Promise.reject(error));
      }
    } while (true);
  }
}
