import { AggregateRoot, IEvent } from '@nestjs/cqrs';

export abstract class CommitRoot extends AggregateRoot {
  public abstract onEvent(event: IEvent): void;

  constructor(public aggregateId: string, public version: number = 1) {
    super();
    // tslint:disable-next-line:no-string-literal
    this['getEventHandler'] = () => this['onEvent'];
  }

  protected _getEventName(event: IEvent): string {
    // tslint:disable-next-line:no-string-literal
    return super['getEventName'](event);
  }
}
