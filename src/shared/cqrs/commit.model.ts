import { Schema, Document, SchemaDefinition } from 'mongoose';
import { IEvent } from '@nestjs/cqrs';

export interface SerializedEvent extends IEvent {
  type: string;
}

export interface CommitModel extends Document {
  aggregateId: string;
  version: number;
  events: SerializedEvent[];
}

export function createCommitSchema(definition: SchemaDefinition = {}) {
  const result = new Schema(
    {
      aggregateId: String,
      timestamp: { type: Date, default: Date.now },
      version: Number,
      events: [],
      ...definition,
    },
    { versionKey: false },
  );
  result.index({ aggregateId: 1, version: 1 }, { unique: true });
  return result;
}
