import { RmqOptions, Transport } from '@nestjs/microservices';
if (!process.env.RABBIT_URL) {
  throw new Error('Invalid RABBIT_URL environment variable!');
}
if (!process.env.RABBIT_QUEUE) {
  throw new Error('Invalid RABBIT_URL environment variable!');
}
export const RABBIT_OPTIONS: RmqOptions = {
  transport: Transport.RMQ,
  options: {
    urls: [process.env.RABBIT_URL],
    queue: process.env.RABBIT_QUEUE,
    queueOptions: { durable: false },
  },
};
